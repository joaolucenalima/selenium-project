import time

from selenium import webdriver
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from selenium.webdriver.edge.service import Service

edgeService = Service(EdgeChromiumDriverManager().install())

driver = webdriver.Edge(service=edgeService)

driver.get("https://www.amazon.com.br/")

driver.find_element('xpath', '//*[@id="documentation"]/a').click()

time.sleep(20)

driver.find_element('xpath', '//*[@id="id-search-field"]').send_keys('classes')

driver.find_element('xpath', '//*[@id="submit"]').click()

time.sleep(20)