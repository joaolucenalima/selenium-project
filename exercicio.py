import time

from selenium import webdriver
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from selenium.webdriver.edge.service import Service

# instalando driver e iniciando serviço
edgeService = Service(EdgeChromiumDriverManager().install())
driver = webdriver.Edge(service=edgeService)

driver.maximize_window()

# acessando site
driver.get("https://www.amazon.com.br/")

# buscando mouse logitech
driver.find_element('xpath', '//*[@id="twotabsearchtextbox"]').send_keys('mouse logitech')
driver.find_element('xpath', '//*[@id="nav-search-submit-button"]').click()

# pegando todos os produtos
driver.find_element('xpath', '/html/body/div[1]/div[1]/div[1]/div[1]/div/span[1]/div[1]/div[7]/div/div/span/div/div/div[3]/div[1]/h2/a').click()

# adicionando ao carrinho
driver.find_element('xpath', '//*[@id="add-to-cart-button"]').click()

# buscando por teclados logitech
driver.find_element('xpath', '//*[@id="twotabsearchtextbox"]').send_keys('teclado logitech')
driver.find_element('xpath', '//*[@id="nav-search-submit-button"]').click()

# clicando no primeiro elemento
driver.find_element('xpath', '/html/body/div[1]/div[1]/div[1]/div[1]/div/span[1]/div[1]/div[7]/div/div/span/div/div/div[3]/div[1]/h2/a').click()

# adicionando ao carrinho
driver.find_element('xpath', '//*[@id="add-to-cart-button"]').click()
time.sleep(20)